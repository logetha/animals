package bcas.ap.ne.homework;

public class TestAnimal {
	public static void main(String[] args) {

		Kitten kitten = new Kitten();
		TomCat tomCat = new TomCat();
		Dog dog = new Dog();

		kitten.name();
		System.out.println(kitten.legs());
		System.out.println(kitten.behave());

		tomCat.name();
		System.out.println(tomCat.legs());
		System.out.println(tomCat.behave());

		dog.name();
		System.out.println(dog.legs());
		System.out.println(dog.behave());

	}

}
