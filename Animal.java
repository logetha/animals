package bcas.ap.ne.homework;

public interface Animal {
	public void name();

	public int legs();

	public String behave();

}
